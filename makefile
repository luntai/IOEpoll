#!/bin/sh
#exec to run sh command
exec:
	kill -9 129855
#compile and lib parameter
CC = gcc
CXX = g++
LIBS :=
LDFLAGS   :=
DEFINES   :=
INCLUDE  := -I.
CFLAGS     := -g -Wall -O3 $(DEFINES) $(INCLUDE)
CXXFLAGS := $(CFLAGS) -DHAVE_CONFIG_H

#source file
SOURCE := $(wildcard *.c) $(wildcard *.cpp)
OBJS    := $(patsubst %.c,%.o,$(patsubst %.cpp,%.o,$(SOURCE)))

#make 2 exec-app
TARGET := server client

#i think you should do anything here
#下面的基本上不需要做任何改动了
.PHONY : everything objs clean veryclean rebuild

everything : $(TARGET)

all : $(TARGET)

objs : $(OBJS)

rebuild: veryclean everything

clean :
	rm -fr *.so
	rm -fr *.o

veryclean : clean
	rm -fr $(TARGET)

$TARGET : $(OBJS)
	$(CC) $(CXXFLAGS) -o $@ $(OBJS) $(LDFLAGS) $(LIBS)
	